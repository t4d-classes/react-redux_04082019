import React from 'react';

export const About = ({ match, history }) => {

  const goHome = () => {
    history.push('/');
  }

  return <>
    <h1>About</h1>
    ID: {match.params.id}
    <button type="button" onClick={goHome}>Go Home</button>
  </>;
};