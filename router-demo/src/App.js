import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';

import { Home } from './components/Home';
import { About } from './components/About';
import { Contact } from './components/Contact';


class App extends Component {
  render() {
    return (
      <Router>
        <>
          <nav>
            <ul>
              <li><Link to="/">Home</Link></li>
              <li><Link to="/about/2">About</Link></li>
              <li><Link to="/contact">Contact</Link></li>
            </ul>
          </nav>
          <Route path="/" exact component={Home} />
          <Route path="/about/:id" component={About} />
          <Route path="/contact" component={Contact} />
        </>
      </Router>
    );
  }
}

export default App;
