Exercise 16 (do this as a team activity)

1. Download a fresh copy of the class GitLab repository. Do not overwrite your current code. Remember to npm install after downloading the code.

Git URL: https://gitlab.com/t4d-classes/react-redux_04082019

2. Copy the content of index_car_tool.js file to the index.js file replacing the current content.

3. Create a Redux store, reducer(s), action(s) to manage the state of Car Tool.

Consider: Which parts of component state should stay in the component tree? Which parts should be moved to Redux?

4. Connect the Redux store to Color Tool. Ensure the application works.

5. Ensure all functionality works.
