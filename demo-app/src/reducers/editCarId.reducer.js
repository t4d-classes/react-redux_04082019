import { DELETE_CAR, EDIT_CAR, CANCEL_CAR } from '../actions/cars.action';

export const editCarIdReducer = (state = -1, action) => {

  if (action.type === EDIT_CAR) {
    return action.value;
  }

  if (action.type.endsWith('Done') || action.type === CANCEL_CAR || action.type === DELETE_CAR) {
    return -1;
  }

  return state;

};