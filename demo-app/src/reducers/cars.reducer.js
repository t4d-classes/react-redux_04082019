import { APPEND_CAR_REQUEST, APPEND_CAR_DONE, REPLACE_CAR, DELETE_CAR,
  REFRESH_CARS_REQUEST, REFRESH_CARS_DONE } from '../actions/cars.action';

// const carList = [
//   { id: 1, make: 'Ford', model: 'Fusion Hybrid', year: 2018, color: 'blue', price: 24000 },
//   { id: 2, make: 'Tesla', model: 'Model S', year: 2019, color: 'red', price: 120000 },
// ];

export const carsReducer = (state = [], action) => {

  switch (action.type) {
    case REFRESH_CARS_REQUEST:
      return [];
    case REFRESH_CARS_DONE:
      return action.value;
    case APPEND_CAR_REQUEST:
    case APPEND_CAR_DONE:
      return state;
    // case APPEND_CAR:
    //   return state.concat({
    //     ...action.value,
    //     id: Math.max(...state.map(c => c.id), 0) + 1,
    //   });
    case REPLACE_CAR:
      const newCars = state.concat();
      const carIndex = newCars.findIndex(c => c.id === action.value.id);
      newCars[carIndex] = action.value;
      return newCars;
    case DELETE_CAR:
      return state.filter(car => car.id !== action.value);
    default:
      return state;
  }

};