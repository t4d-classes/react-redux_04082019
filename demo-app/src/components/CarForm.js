import React from 'react';

const getInitialCarForm = () => ({
  make: '',
  model: '',
  year: 1900,
  color: '',
  price: 0,
});

export class CarForm extends React.Component {

  // this is not valid JavaScript, this is known as a class property
  state = getInitialCarForm();

  constructor(props) {
    super(props);

    // this.state = getInitialCarForm();
    this.submit = this.submit.bind(this);
  }

  // class arrow function - not valid JavaScript, but the transpiler converts to valid JavaScript
  change = ({ target: { type, name, value }}) => {
    this.setState({
      [ name ]: type === 'number' ? Number(value) : value,
    });
  }

  submit() {
    this.props.onSubmitCar({ ...this.state });
    this.setState(getInitialCarForm());
  }

  render() {
    return <form>
      <div>
        <label htmlFor="make-input">Make:</label>
        <input type="text" id="make-input" value={this.state.make}
          onChange={this.change} name="make" />
      </div>
      <div>
        <label htmlFor="model-input">Model:</label>
        <input type="text" id="model-input" value={this.state.model}
          onChange={this.change} name="model" />
      </div>
      <div>
        <label htmlFor="year-input">Year:</label>
        <input type="number" id="year-input" value={this.state.year}
          onChange={this.change} name="year" />
      </div>
      <div>
        <label htmlFor="color-input">Color:</label>
        <input type="text" id="color-input" value={this.state.color}
          onChange={this.change} name="color" />
      </div>
      <div>
        <label htmlFor="price-input">Price:</label>
        <input type="number" id="price-input" value={this.state.price}
          onChange={this.change} name="price" />
      </div>
      <button type="button" onClick={this.submit}>{this.props.buttonText}</button>
    </form>;
  }
}

