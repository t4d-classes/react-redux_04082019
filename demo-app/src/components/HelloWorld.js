import React from 'react';

// declaring a variable on the stack
// creating a function object on the heap
// assigning the function object reference to variable
export const HelloWorld = () => {
  // return React.createElement('header', null, [
  //   React.createElement(
  //     'h1',
  //     null,
  //     'Hello World!!',
  //   ),
  //   React.createElement(
  //     'span',
  //     null,
  //     'test',
  //   )
  // ]);

  return <header>
    <h1>Hello World!!</h1>
    <span>test</span>
  </header>;
};

// invoking the function
// HelloWorld();