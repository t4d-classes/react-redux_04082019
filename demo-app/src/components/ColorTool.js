import React, { useState } from 'react';

import { ToolHeader } from './ToolHeader';
import { ColorList } from './ColorList';
import { ColorForm } from './ColorForm';

export const ColorTool = ({ headerText, colors: initialColors }) => {

  const [ colors, setColors ] = useState(initialColors.concat());

  const addColor = color => {
    setColors(colors.concat({
      ...color,
      id: Math.max(...colors.map(c => c.id), 0) + 1,
    }));
  };

  return <>
    <ToolHeader headerText={headerText} />
    <ColorList colors={colors} />
    <ColorForm buttonText="Add Color" onSubmitColor={addColor} />
  </>;
};

ColorTool.defaultProps = {
  headerText: 'Color Tool',
  colors: [],
};
