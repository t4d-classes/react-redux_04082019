import React from 'react';

export const ColorList = ({ colors }) => {

  return <ul>
    {colors.map(color =>
      <li key={color.id}>
        {color.name} - {color.hexCode}
      </li>
    )}
  </ul>;

};