import React, { useState } from 'react';

export const ColorForm = ({ buttonText, onSubmitColor }) => {

  const [ colorForm, setColorForm ] = useState({
    name: '',
    hexCode: '',
  });

  const change = e => {
    setColorForm({
      ...colorForm,
      [ e.target.name ]: e.target.value,
    });
  };

  const submitColor = () => {

    onSubmitColor({
      ...colorForm,
    });

    setColorForm({
      name: '',
      hexCode: '',
    });

  }

  return <form>
    <div>
      <label htmlFor="color-name-input">Color Name:</label>
      <input type="text" id="color-name-input" value={colorForm.name} name="name" onChange={change} />
    </div>
    <div>
      <label htmlFor="color-input">Color HexCode:</label>
      <input type="text" id="color-input" value={colorForm.hexCode} name="hexCode" onChange={change} />
    </div>
    <button type="button" onClick={submitColor}>{buttonText}</button>
  </form>;

};