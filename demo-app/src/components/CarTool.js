import React, { useEffect } from 'react';
import PropTypes from 'prop-types';

import { ToolHeader } from './ToolHeader';
import { CarTable } from './CarTable';
import { CarForm } from './CarForm';

export const CarTool = ({
  headerText, cars, editCarId, onAddCar, onSaveCar,
  onDeleteCar, onEditCar, onCancelCar, onRefreshCars,
}) => {

  useEffect(() => {
    onRefreshCars();
  }, []);

  return <>
    <ToolHeader headerText={headerText} />
    <CarTable cars={cars} onDeleteCar={onDeleteCar} editCarId={editCarId}
      onSaveCar={onSaveCar} onCancelCar={onCancelCar} onEditCar={onEditCar} />
    <CarForm buttonText="Add Car" onSubmitCar={onAddCar} />
  </>;
};

CarTool.defaultProps = {
  headerText: 'Car Tool',
  cars: [],
}

CarTool.propTypes = {
  headerText: PropTypes.string.isRequired,
  cars: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number,
    make: PropTypes.string,
    model: PropTypes.string,
    year: PropTypes.number,
    color: PropTypes.string,
    price: PropTypes.number,
  })).isRequired,
};