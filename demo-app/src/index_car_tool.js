import React from 'react';
import ReactDOM from 'react-dom';

import { CarTool } from './components/CarTool';

const carList = [
  { id: 1, make: 'Ford', model: 'Fusion Hybrid', year: 2018, color: 'blue', price: 24000 },
  { id: 2, make: 'Tesla', model: 'Model S', year: 2019, color: 'red', price: 120000 },
];

ReactDOM.render(
  <CarTool cars={carList} headerText="Car Tool" />,
  document.querySelector('#root'),
);

