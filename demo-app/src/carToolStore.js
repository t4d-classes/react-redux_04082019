import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';

import { carsReducer } from './reducers/cars.reducer';
import { editCarIdReducer } from './reducers/editCarId.reducer';


export const carToolStore = createStore(combineReducers({
  cars: carsReducer,
  editCarId: editCarIdReducer,
}), composeWithDevTools(applyMiddleware(thunk)));