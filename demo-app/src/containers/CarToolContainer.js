import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { CarTool } from '../components/CarTool';
import { createReplaceCarAction, createDeleteCarAction,
  createEditCarAction, createCancelCarAction, refreshCars, appendCar, replaceCar } from '../actions/cars.action';

export const CarToolContainer = connect(
  ({ cars, editCarId }) => ({ cars, editCarId }),
  dispatch => bindActionCreators({
    onRefreshCars: refreshCars,
    onAddCar: appendCar,
    onSaveCar: replaceCar,
    onDeleteCar: createDeleteCarAction,
    onEditCar: createEditCarAction,
    onCancelCar: createCancelCarAction,
  }, dispatch),
)(CarTool);