import React from 'react';
import ReactDOM from 'react-dom';

import { ColorTool } from './components/ColorTool';

const colorList = [
  { id: 1, name: 'purple', hexCode: '#AAAAAA' },
  { id: 2, name: 'green', hexCode: '#AAAAAA' },
  { id: 3, name: 'teal', hexCode: '#AAAAAA' },
  { id: 4, name: 'red', hexCode: '#AAAAAA' },
];

ReactDOM.render(
  <ColorTool colors={colorList} headerText="Color Tool" />,
  document.querySelector('#root'),
);

