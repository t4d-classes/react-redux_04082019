export const APPEND_CAR = '[Car Tool] Append Car';
export const REPLACE_CAR = '[Car Tool] Replace Car';
export const DELETE_CAR = '[Car Tool] Delete Car';
export const EDIT_CAR = '[Car Tool] Edit Car';
export const CANCEL_CAR = '[Car Tool] Cancel Car';

export const createAppendCarAction = value => ({ type: APPEND_CAR, value });
export const createReplaceCarAction = value => ({ type: REPLACE_CAR, value });
export const createDeleteCarAction = value => ({ type: DELETE_CAR, value });
export const createEditCarAction = value => ({ type: EDIT_CAR, value });
export const createCancelCarAction = () => ({ type: CANCEL_CAR });

export const REFRESH_CARS_REQUEST = '[Car Tool] Refresh Cars Request';
export const REFRESH_CARS_DONE = '[Car Tool] Refresh Cars Done';

export const createRefreshCarsRequestAction = () => ({ type: REFRESH_CARS_REQUEST });
export const createRefreshCarsDoneAction = (value) => ({ type: REFRESH_CARS_DONE, value });

export const refreshCars = () => {

  // return async dispatch => {
  //   dispatch(createRefreshCarsRequestAction());
  //   const res = await fetch('http://localhost:3050/cars');
  //   const cars = await res.json();
  //   return dispatch(createRefreshCarsDoneAction(cars));
  // };

  return dispatch => {
    dispatch(createRefreshCarsRequestAction());
    return fetch('http://localhost:3050/cars')
      .then(res => res.json())
      .then(cars => dispatch(createRefreshCarsDoneAction(cars)));
  };

};

export const APPEND_CAR_REQUEST = '[Car Tool] Append Car Request';
export const APPEND_CAR_DONE = '[Car Tool] Append Car Done';

export const createAppendCarRequestAction = (value) => ({ type: APPEND_CAR_REQUEST, value });
export const createAppendCarDoneAction = (value) => ({ type: APPEND_CAR_DONE, value });


export const appendCar = (car) => {

  return dispatch => {
    dispatch(createAppendCarRequestAction(car));
    return fetch('http://localhost:3050/cars', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(car),
    })
      .then(car => dispatch(refreshCars()));
  };

};

export const REPLACE_CAR_REQUEST = '[Car Tool] Replace Car Request';
export const REPLACE_CAR_DONE = '[Car Tool] Replace Car Done';

export const createReplaceCarRequestAction = (value) => ({ type: REPLACE_CAR_REQUEST, value });
export const createReplaceCarDoneAction = (value) => ({ type: REPLACE_CAR_DONE, value });


export const replaceCar = (car) => {

  return dispatch => {
    dispatch(createReplaceCarRequestAction(car));
    return fetch(`http://localhost:3050/cars/${encodeURIComponent(car.id)}`, {
      method: 'PUT',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(car),
    })
      .then(car => dispatch(refreshCars()));
  };

};