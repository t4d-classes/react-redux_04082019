import React, { useState } from 'react';
import ReactDOM from 'react-dom';
import { createStore, bindActionCreators } from 'redux';
import { connect, Provider } from 'react-redux';

const ADD = '[CalcAction] ADD';
const SUBTRACT = '[CalcAction] SUBTRACT';
const MULTIPLY = '[CalcAction] MULTIPLY';
const DIVIDE = '[CalcAction] DIVIDE';
const CLEAR = '[CalcAction] CLEAR';

const createAddAction = (value) => ({ type: ADD, value });
const createSubtractAction = (value) => ({ type: SUBTRACT, value });
const createMultiplyAction = (value) => ({ type: MULTIPLY, value });
const createDivideAction = (value) => ({ type: DIVIDE, value });
const createClearAction = () => ({ type: CLEAR });

// Pure Function
// 1. The only data used in the function is passed via parameters
// 2. Do not mutate the parameters
// 3. Cannot perform any side effects
// 4. The only return value is returned with the return statement
const calcReducer = (state = { result: 0, opHistory: [] }, action) => {
  switch (action.type) {
    case CLEAR:
      return {
        ...state,
        result: 0,
        opHistory: [],
      };
    case ADD:
      return {
        ...state,
        result: state.result + action.value,
        opHistory: state.opHistory.concat({ name: action.type, value: action.value }),
      };
    case SUBTRACT:
      return {
        ...state,
        result: state.result - action.value,
        opHistory: state.opHistory.concat({ name: action.type, value: action.value }),
      };
    case MULTIPLY:
      return {
        ...state,
        result: state.result * action.value,
        opHistory: state.opHistory.concat({ name: action.type, value: action.value }),
      };
    case DIVIDE:
      return {
        ...state,
        result: state.result / action.value,
        opHistory: state.opHistory.concat({ name: action.type, value: action.value }),
      };
    default:
      return state;
  }
};

// const createStore = (reducerFn) => {

//   let currentState = undefined;
//   const subscriberFns = [];

//   return {
//     getState: () => currentState,
//     dispatch: (action) => {
//       currentState = reducerFn(currentState, action);
//       subscriberFns.forEach(fn => fn());
//     },
//     subscribe: (subscriberFn) => subscriberFns.push(subscriberFn),
//   };

// };

const calcStore = createStore(calcReducer);

// calcStore.subscribe(() => {
//   ReactDOM.render(<CalcTool result={calcStore.getState().result}
//     opHistory={calcStore.getState().opHistory}
//     onAdd={add} onSubtract={subtract} onClear={clear}
//     onMultiply={multiply} onDivide={divide} />, document.querySelector('#root'));
// });


// const add = (value) => calcStore.dispatch(createAddAction(value));
// const subtract = (value) => calcStore.dispatch(createSubtractAction(value));
// const multiply = (value) => calcStore.dispatch(createMultiplyAction(value));
// const divide = (value) => calcStore.dispatch(createDivideAction(value));
// const clear = () => calcStore.dispatch(createClearAction());

// const { add, subtract, multiply, divide, clear } = bindActionCreators({
//   add: createAddAction,
//   subtract: createSubtractAction,
//   multiply: createMultiplyAction,
//   divide: createDivideAction,
//   clear: createClearAction,
// }, calcStore.dispatch);



// add(1);
// subtract(2);
// add(3);
// subtract(4);
// add(5);
// multiply(4);
// divide(2);

const CalcTool = ({ result, opHistory, onAdd, onSubtract, onMultiply, onDivide, onClear }) => {

  // the state for the final result is managed in Redux
  // const [ result, setResult ] = useState(0);
  const [ numInput, setNumInput ] = useState(0);

  const change = (e) => setNumInput(Number(e.target.value));

  // the logic for performing the operations is in the reducer
  // const add = () => setResult(result + numInput);
  // const subtract = () => setResult(result - numInput);
  // const multiply = () => setResult(result * numInput);
  // const divide = () => setResult(result / numInput);

  // onAdd function both creates the action and dispatches it into the store
  const add = () => onAdd(numInput);
  const subtract = () => onSubtract(numInput);
  const multiply = () => onMultiply(numInput);
  const divide = () => onDivide(numInput);

  return <form>
    <div>
      Result: {result}
    </div>
    <div>
      <label htmlFor="num-input">Input:</label>
      <input type="number" id="num-input" value={numInput} onChange={change} />
    </div>
    <div>
      <button type="button" onClick={add}>+</button>
      <button type="button" onClick={subtract}>-</button>
      <button type="button" onClick={multiply}>*</button>
      <button type="button" onClick={divide}>/</button>
    </div>
    <ul>
      {opHistory.map(opEntry => <li>{opEntry.name} {opEntry.value}</li>)}
    </ul>
    <button type="button" onClick={onClear}>Clear</button>
  </form>
};

const mapStateToProps = (state) => ({
  result: state.result,
  opHistory: state.opHistory,
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators({
    onAdd: createAddAction,
    onSubtract: createSubtractAction,
    onMultiply: createMultiplyAction,
    onDivide: createDivideAction,
    onClear: createClearAction,
  }, dispatch);

const CalcToolContainer = connect(mapStateToProps, mapDispatchToProps)(CalcTool);

ReactDOM.render(
  <Provider store={calcStore}>
    <CalcToolContainer />
  </Provider>,
  document.querySelector('#root'),
);

